// ==UserScript==
// @name            [ALL] Block Are You Sure You Want To Leave This Page
// @author
// @description     Block "Are you sure you want to leave this page" popup.
// @downloadURL
// @grant
// @homepageURL     https://bitbucket.org/INSMODSCUM/userscripts-scripts/src
// @icon
// @include         http*://*
// @namespace       insmodscum
// @require
// @run-at          document-start
// @updateURL
// @version         1.0
// ==/UserScript==

window.onbeforeunload = null;