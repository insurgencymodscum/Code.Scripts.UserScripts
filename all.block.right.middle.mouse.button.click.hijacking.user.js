// ==UserScript==
// @name            [ALL] Block Right & Middle Mouse Button Click Hijacking
// @author
// @description     Block mouse button click hijacking.
// @downloadURL
// @grant
// @homepageURL     https://bitbucket.org/INSMODSCUM/userscripts-scripts/src
// @icon
// @include         http*://*
// @namespace       insmodscum
// @require
// @run-at          document-start
// @updateURL
// @version         1.0
// ==/UserScript==

document.body.removeAttribute ("oncontextmenu");
unsafeWindow.document.oncontextmenu = false;
unsafeWindow.document.oncontextmenu = null;

// source: https://greasyfork.org/en/scripts/12434-prevent-middle-click-hijacking/code
// http://www.w3schools.com/jsref/event_button.asp

// 0 : Left mouse button
// 1 : Wheel button or middle button (if present)
// 2 : Right mouse button

function handler(e){
    if((e.button === 0 && e.ctrlKey) || e.button == 1 || e.button == 2 ){
        e.stopPropagation();
    }
}

addEventListener('click', handler, true);
addEventListener('mousedown', handler, true);
addEventListener('mouseup', handler, true);